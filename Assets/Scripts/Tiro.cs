﻿using UnityEngine;

public class Tiro : MonoBehaviour
{
    public float rate = 1;
    public float speed = 5;
    public float damage = 1;

    void Update()
    {
        Vector3 pos = transform.position;
        pos.y += Time.deltaTime * speed;
        transform.position = pos;
    }
}
