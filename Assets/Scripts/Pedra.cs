﻿using UnityEngine;

public class Pedra : MonoBehaviour
{
    public int points = 100;
    public float life = 3;
    public float gravityScale = 0.5f;
    public GameObject explosion;

    private void Awake()
    {
        
        float gs = Random.Range(.001f, .3f);
        GetComponent<Rigidbody2D>().gravityScale = gs;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Tiro tiro = collision.GetComponent<Tiro>();
        if (tiro)
        {
            life -= tiro.damage;

            // Explosion
            if (life < 1)
            {
                FindObjectOfType<Score>().UpdateScore(points);

                GameObject boom =
                Instantiate(
                    explosion,
                    transform.position,
                    transform.rotation);

                Destroy(boom, 3);
                Destroy(gameObject);
            }

            Destroy(tiro.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        
    }
}
