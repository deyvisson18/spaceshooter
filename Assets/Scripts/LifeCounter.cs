﻿using UnityEngine;
using UnityEngine.UI;

public class LifeCounter : MonoBehaviour
{
    Text contador;

    private void Awake()
    {
        contador = GetComponent<Text>();
    }

    public void UpdateValue (int value)
    {
        contador.text = value.ToString();
    }
}
