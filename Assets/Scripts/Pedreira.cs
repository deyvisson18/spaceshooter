﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pedreira : MonoBehaviour
{
    public GameObject[] pedras;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Pedrada());
    }

    IEnumerator Pedrada ()
    {
        float time = Random.Range(0.1f, 2);
        yield return new WaitForSeconds(time);

        int index = Random.Range(0, pedras.Length - 1);
        Vector3 position = transform.position;
        position.x = Random.Range(-2.5f, 2.5f);

        GameObject pedra = 
            Instantiate(
                pedras[index], 
                position, 
                transform.rotation);

        Destroy(pedra, 20);

        StartCoroutine(Pedrada());
    }
}
